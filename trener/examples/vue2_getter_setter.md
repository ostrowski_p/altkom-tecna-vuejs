https://www.how-to-vue.com/vue/reactivity/observer.html#the-observe-method

```js

obj = {
    _secret:'',
    get value(){ 
        return this._secret.split('').reverse().join('')
    },
    set value(v){
        this._secret = v.split('').reverse().join('')
    },
}
// {_secret: ''}
obj.value = 'Ala ma kota'
// 'Ala ma kota'
obj
// {_secret: 'atok am alA'}
obj.value
// 'Ala ma kota'
obj2 = Vue.observable({ value:'test', message:'placki' })
// {__ob__: Observer}
//     message: (...)
//     value: (...)
//     __ob__: Observer
//         dep: Dep
//         id: 4
//         subs: []
//             [[Prototype]]: Object
//             value: {__ob__: Observer}
//             vmCount: 0
obj2.__ob__.dep.addSub({update: console.log})
// undefined
obj2.message = 'zmiana'
'zmiana'

Vue.set(obj2,'nowawartost','test')
// 'test'

```

```js
app.nested
// {__ob__: Observer}
app.nested = {myValue:123, placki:'test'}
// {__ob__: Observer}
app.nested 
// {__ob__: Observer}
// myValue: (...)
// placki: (...)
```