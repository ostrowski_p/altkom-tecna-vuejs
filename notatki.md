## Instalacje

node -v 

npm -v 

git --version 

code -v 

chome://version
Google Chrome	97.0.4692.99

## GIT 
<!-- git clone https://bitbucket.org/ev45ive/altkom-tecna-vuejs.git altkom-tecna-vuejs -->

https://bitbucket.org/account/settings/app-passwords/ <- haslo do pushowania :-)

https://bitbucket.org/ev45ive/altkom-tecna-vuejs.git > Zaloguj > Plus (po prawej) -> Fork this repository (Public)

git clone https://bitbucket.org/<WASZ_LOGIN>/altkom-tecna-vuejs.git altkom-tecna-vuejs

cd altkom-tecna-vuejs

git remote add trener https://bitbucket.org/ev45ive/altkom-tecna-vuejs.git

git pull trener master

mkdir imienazwisko

## Vue CLI

https://cli.vuejs.org/
npm uninstall -g @vue/cli

npm install -g @vue/cli
npm install -g @vue/cli@latest
npm install -g @vue/cli@4.5.15

vue --version
@vue/cli 4.5.15

vue create vue-spa


Vue CLI v4.5.15
? Please pick a preset: Manually select features
? Check the features needed for your project: Choose Vue version, Babel, TS, PWA, Router, Vuex, Linter, Unit, E2E
? Choose a version of Vue.js that you want to start the project with 3.x
? Use class-style component syntax? Yes
? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? Yes
? Use history mode for router? (Requires proper server setup for index fallback in production) No
? Pick a linter / formatter config: Basic
? Pick additional lint features: Lint on save   
? Pick a unit testing solution: Jest
? Pick an E2E testing solution: Cypress
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: tecna-vue3

🎉  Preset tecna-vue3 saved in C:\Users\PC\.vuerc


Vue CLI v4.5.15
✨  Creating project in C:\Projects\szkolenia\altkom-tecna-vuejs\trener\vue-spa.
⚙️  Installing CLI plugins. This might take a while...

⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project vue-spa.
👉  Get started with the following commands:

 $ cd vue-spa
 $ npm run serve

 ## Npm serve
 npm run serve

 DONE  Compiled successfully in 5140ms                                                                                           11:34:02 AM

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.1.107:8080/
Note that the development build is not optimized.
  To create a production build, run npm run build.

No issues found.

## Vue Devtools
v2 https://devtools.vuejs.org/guide/installation.html
v3 https://chrome.google.com/webstore/detail/vuejs-devtools/ljjemllljcmogpfapbkkighbhhppjdbg

## Bootstrap
npm i bootstrap

## UI toolkits
https://vuetifyjs.com/en/
https://primefaces.org/primevue/showcase/#/ 
https://www.naiveui.com/en-US/os-theme/docs/usage-sfc
https://bootstrap-vue.org/docs/components/input-group
https://antdv.com/docs/vue/introduce/

## Storybook
npx sb init

## VS Code extensions
https://marketplace.visualstudio.com/items?itemName=hollowtree.vue-snippets
https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar
https://marketplace.visualstudio.com/items?itemName=octref.vetur

## CSS in JS
https://developer.mozilla.org/pl/docs/Web/CSS/Using_CSS_custom_properties

https://sass-lang.com/guide - static / precompiled

https://cssinjs.org/?v=v10.9.0 
https://www.npmjs.com/package/vue-styled-components

