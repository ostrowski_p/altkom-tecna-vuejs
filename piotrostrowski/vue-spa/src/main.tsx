import { createApp, h } from "vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import "bootstrap/dist/css/bootstrap.css";
import { Vue } from "vue-class-component";

// createApp(App)
// const FuncComp = ({ message }: { message: string }) => h('div', {}, [
//     h('h1', { class: "display-1" }, 'Hello'),
//     h('input', {}),
//     h('p', { id: 'test' }, message),
// ]);
const FuncComp = ({ message }: { message: string }) => (
  <div>
    <h1 class="display-1">Hello</h1>
    <p>{message}</p>
  </div>
);

(window as any).app = createApp({
  data() {
    return { message: "Hello from render method" };
  },
  render() {
    return h("div", { class: "container" }, [
      h("div", { class: "row" }, [
        h("div", { class: "col" }, [FuncComp({ message: this.message })]),
        h(FuncComp, { message: this.message }, []),
      ]),
    ]);
  },
})
  .use(store)
  .use(router)
  .mount("#app");
